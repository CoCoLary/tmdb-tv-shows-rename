# TMDb TV shows rename

Table of contents :
1. [Description](#description)
2. [searchTVShows](#searchtvshows)
3. [buildTVShow](#buildtvshow)
4. [Suggestion of use](#suggestion-of-use)

## Description
This project allow you to quickly rename your TV show files using the [TMDb API](https://developers.themoviedb.org).
It is composed of two Python scripts :
1. [searchTVShows.py](#searchtvshows)
2. [buildTVShow.py](#buildtvshow)

These scripts use an API key stored in a plain text file. You can get yours in your [account settings](https://www.themoviedb.org/settings/api).

## searchTVShows
This script is used to search for a TV show ID. This ID is required with the [buildTVShow](#buildtvshow) script. Results are given in decreasing popularity order.

Help :
```sh
python searchTVShows.py --help
```
```
usage: searchTVShows.py [-h] [--debug] [-k FILE] [-l LANGUAGE] [-p [1-1000]]
                        [-a] [-y YEAR]
                        TITLE

positional arguments:
  TITLE                 First air date year

optional arguments:
  -h, --help            show this help message and exit
  --debug               Enable debugging output
  -k FILE, --api-key FILE
                        Path to file containing plain text TMDB API key.
                        Default : api_key
  -l LANGUAGE, --language LANGUAGE
                        Pass a ISO 639-1 value to display translated data for
                        the fields that support it. Default based on system's
                        locale : "fr-FR".
  -p [1-1000], --page [1-1000]
                        Specify which page to query. Default : 1
  -a, --adult           Choose whether to inlcude adult (pornography) content
                        in the results. Default : disabled
  -y YEAR, --year YEAR  First air date year
```

## buildTVShow
This script is used to build the TV show directories, TV show and seasons, and rename files.
**IMPORTANT : The script renames files using alphabetical order. Be sure that your season are complete and ordered before use the script. You might want to create empty file to fill the holes.**

Help :
```sh
python buildTVShow.py --help
```
```
usage: buildTVShow.py [-h] [--debug] [-v] [-k FILE] [-l LANGUAGE] ID [PATH]

positional arguments:
  ID                    TV show ID
  PATH                  Path to directory to create TV show directory

optional arguments:
  -h, --help            show this help message and exit
  --debug               Enable debugging output
  -v, --verbose         Enable verbose output
  -k FILE, --api-key FILE
                        Path to file containing plain text TMDB API key.
                        Default : api_key
  -l LANGUAGE, --language LANGUAGE
                        Pass a ISO 639-1 value to display translated data for
                        the fields that support it. Default based on system's
                        locale : "fr-FR".
```

## Suggestion of use
1. Get the ID of the show you are looking for :
```sh
python searchTVShows.py "sherlock" -y 2010
```
It will return one result :
```
Sherlock (2010) :
                    id : **19885**
         original name : Sherlock
        origin country : GB
```

2. Init the directories :
```sh
python buildTVShow.py 19885 path/to/parent/directory/
```

3. Place your video files in the right season directory paying attention to the file alphabetical order.
4. Rename the files using the previous command :
```sh
python buildTVShow.py 19885 path/to/parent/directory/
```