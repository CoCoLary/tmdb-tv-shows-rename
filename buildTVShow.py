
import sys
import locale
import argparse
import requests
import json
import os
import re


def _debug( text : str, **kwargs ) :
	# if args is set in function or in global and if debug is true
	if "args" in vars() or "args" in globals() and args.debug == True :
		# if kwargs is set
		if kwargs :
			print( text, **kwargs )
		else :
			print( text )

def _verbose( text : str, **kwargs ) :
	# if args is set in function or in global and if verbose is true
	if "args" in vars() or "args" in globals() and args.verbose == True :
		# if kwargs is set
		if kwargs :
			print( text, **kwargs )
		else :
			print( text )

def _error( text : str, **kwargs ) :
	# if kwargs is set
	if kwargs :
		print( text, file = sys.stderr, **kwargs )
	else :
		print( text, file = sys.stderr )

def _getISO_639_1() :
	language, encoding = locale.getdefaultlocale()
	iso_639_1 = language.replace( "_", "-" )

	return iso_639_1

def _parseArgs() :
	# Getting locale
	iso_639_1 = _getISO_639_1()

	# Defining args
	argParser = argparse.ArgumentParser()
	argParser.add_argument( "--debug", action="store_true", default = False, help = "Enable debugging output" )
	argParser.add_argument( "-v", "--verbose", action="store_true", default = False, help = "Enable verbose output" )
	argParser.add_argument( "-k", "--api-key", metavar = "FILE", type = str, default = "api_key", help = "Path to file containing plain text TMDB API key. Default : api_key" )
	argParser.add_argument( "-l", "--language", metavar = "LANGUAGE", type = str, default = iso_639_1, help = "Pass a ISO 639-1 value to display translated data for the fields that support it. Default based on system's locale : \"%s\"." % iso_639_1 )

	argParser.add_argument( "id", metavar = "ID", type = int, help = "TV show ID" )
	argParser.add_argument( "destination", nargs = "?", metavar = "PATH", type = str, default = "", help = "Path to directory to create TV show directory" )

	# Parsing args
	args = argParser.parse_args()

	return args

def getSeasonDetails( apiKey, showId, seasonNumber, language ) :
	# https://developers.themoviedb.org/3/tv-seasons/get-tv-season-details
	query = "https://api.themoviedb.org/3/tv/%d/season/%d?" % ( showId, seasonNumber )

	# Required
	query += "api_key=%s" % apiKey

	# Optional
	if language :
		query += "&language=%s" % language

	data = None
	r = requests.get( query )
	if r.status_code == 200 :
		data = json.loads( r.text )

	if data :
		episodes = []
		if "episodes" in data and data["episodes"] :
			for episode in data["episodes"] :
				episodeDetails = {}
				if "episode_number" in episode and episode["episode_number"] :
					episodeDetails["episode_number"] = int( episode["episode_number"] )

				if "name" in episode and episode["name"] :
					episodeDetails["name"] = episode["name"].strip()

				episodes.append( episodeDetails )
	else :
		episodes = None

	return episodes

def getShowDetails( apiKey, showId, language = None ) :
	# https://developers.themoviedb.org/3/tv/get-tv-details
	query = "https://api.themoviedb.org/3/tv/%d?" % showId

	# Required
	query += "&api_key=%s" % apiKey

	# Optional
	if language :
		query += "&language=%s" % language

	data = None
	r = requests.get( query )
	if r.status_code == 200 :
		data = json.loads( r.text )

	if data :
		showDetails = {}
		if "name" in data and data["name"] :
			showDetails["name"] = data["name"].strip()

		if "first_air_date" in data and data["first_air_date"] :
			showDetails["first_air_date"] = data["first_air_date"].strip()

		if "number_of_seasons" in data and data["number_of_seasons"] :
			showDetails["number_of_seasons"] = int( data["number_of_seasons"] )

		showDetails["seasons"] = []
		if "seasons" in data and data["seasons"] :
			for season in data["seasons"] :
				seasonDetails = {}
				if "season_number" in season and season["season_number"] :
					seasonDetails["season_number"] = int( season["season_number"] )
					seasonDetails["episodes"] = getSeasonDetails( apiKey, showId, seasonDetails["season_number"], language )

				if "name" in season and season["name"] :
					seasonDetails["name"] = season["name"].strip()

				if "episode_count" in season and season["episode_count"] :
					seasonDetails["episode_count"] = int( season["episode_count"] )


				showDetails["seasons"].append( seasonDetails )
	else :
		showDetails = None

	return showDetails

def cleanfileName( name ) :
	cleaned = re.sub( "[\/\:\*\?\"\<\>\|\t\n]+", "", name )
	cleaned = re.sub( "[ ]+", " ", cleaned )

	return cleaned

def makeDir( path ) :
	errno = 0
	# if path is not a directory
	if not os.path.isdir( path ) :
		# if path does'nt exist
		if not os.path.exists( path ) :
			_verbose( "Creating \"%s\" directory..." % path )
			os.makedirs( path )
		else :
			_error( "\"%s\" already exists and is not a directory" % path )
			errno = 1
	else :
		_verbose( "\"%s\" directory already exists. Passing..." % path )

	return errno

def _getEpisodeNumber( jsonObj ) :
	if "episode_number" in jsonObj :
		v = int( jsonObj["episode_number"] )
	else :
		v = 0

	return v

def buildShowDirectory( data, path ) :
	errno = 0

	# Determining show directory absolute path
	if errno == 0 :
		if "name" in data and data["name"] :
			rootPath = os.path.abspath( path )
			showDirectory = data["name"].strip()
			showDirectory += " (%d)" % int( data["first_air_date"].split( "-" )[0] )
			showDirectory = cleanfileName( showDirectory )

			showAbsPath = os.path.join( rootPath, showDirectory )
			_debug( "showAbsPath = %s" % showAbsPath )
		else :
			_error( "No show name found")
			errno = 1

	# Creating show directory
	if errno == 0 :
		errno = makeDir( showAbsPath )

	# Processing show seasons
	if errno == 0 :
		if "seasons" in data and data["seasons"] :
			for season in data["seasons"] :
				if "name" in season and season["name"] :
					_verbose( "Processing season : %s" % season["name"] )
					seasonDirectory = cleanfileName( season["name"] )
					# Creating season directory
					seasonAbsPath = os.path.join( showAbsPath, seasonDirectory )
					errno = makeDir( seasonAbsPath )

					# Processing season episodes
					if "episodes" in season and season["episodes"] :
						season["episodes"].sort( key = _getEpisodeNumber, reverse = False )
						files = [f for f in os.listdir( seasonAbsPath ) if os.path.isfile( os.path.join( seasonAbsPath, f ))]
						files.sort( reverse = False )
						_debug( "files in \"%s\" : %s" % ( seasonAbsPath, files ))
						for episode, oldEpisodeName in zip( season["episodes"], files ) :
							if "name" in episode and episode["name"] :
								fExt = os.path.splitext( oldEpisodeName )[1]
								newEpisodeName = "%02dx%03d_%s%s" % ( season["season_number"], episode["episode_number"], cleanfileName( episode["name"] ), fExt )

								if oldEpisodeName != newEpisodeName :
									_verbose( "\tRenaming \"%s\" to \"%s\"..." % ( oldEpisodeName, newEpisodeName ))
									oldEpisodeAbsPath = os.path.join( seasonAbsPath, oldEpisodeName )
									newEpisodeAbsPath = os.path.join( seasonAbsPath, newEpisodeName )
									os.rename( oldEpisodeAbsPath, newEpisodeAbsPath )
								else :
									_verbose( "\t\"%s\" already well named. Passing..." % oldEpisodeName )
							
		else :
			_error( "No seasons found" )
			errno = 1

if __name__ == "__main__" :
	errno = 0

	# Getting locale
	if errno == 0 :
		iso_639_1 = _getISO_639_1()

	# Process arguments
	if errno == 0 :
		args = _parseArgs()
		_debug( args )

	# Getting API key
	if errno == 0 :
		apiKey = None
		with open( args.api_key ) as keyFile :
			apiKey = keyFile.read()
		_debug( "key = %s" % apiKey )

	# Requesting API
	if errno == 0 :
		data = getShowDetails( apiKey, args.id, args.language )
		_debug( "Show details :\n%s" % json.dumps( data, indent = 4, ensure_ascii = False ))
		if not data :
			errno = 1

	# Building directory
	if errno == 0 :
		buildShowDirectory( data, args.destination )

	# Exit
	sys.exit( errno )
